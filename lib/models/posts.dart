import 'dart:convert';

///Post class to send requests
class PostRequestOrder {
  final String idPlace;
  final List<String> idInQueue;

  PostRequestOrder({this.idPlace, this.idInQueue});

  factory PostRequestOrder.fromJson(Map<String, dynamic> json) {
    return PostRequestOrder(
      idPlace: json['idPlace'],
      idInQueue: json['idInQueue']
    );
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map['idPlace'] = idPlace;
    map['idInQueue'] = json.encode(idInQueue) ;
    return map;
  }
}

///Post class to send requests
class PostUpdateOrder {
  final bool seen = true;

  Map toMap() {
    var map = new Map<String, dynamic>();
    map['seen'] = seen;
    return map;
  }
}

///Post class to send orders
class PostLogin {
  final String username;
  final String password;
  final String strategy;

  PostLogin({this.username, this.password, this.strategy});

  factory PostLogin.fromJson(Map<String, dynamic> json) {
    return PostLogin(
        username: json['username'], password: json['password'], strategy: json['strategy']
    );
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["username"] = username;
    map["password"] = password;
    map["strategy"] = strategy;
    return map;
  }
}