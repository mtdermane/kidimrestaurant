
class OrderDetail {
  String _placeId;
  bool _seen;
  String _table;
  List _order;
  String _id;
  bool isDeleted = false;

  OrderDetail(this._id, this._placeId, this._seen, this._table, this._order);

  String get placeId => _placeId;

  List get order => _order;

  String get table => _table;

  String get id => _id;

  bool get seen => _seen;

  @override
  bool operator == (Object o) => o is OrderDetail && _id == o.id;

}