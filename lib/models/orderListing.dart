class OrderListing {
  String _tableNumber;
  String _id;
  bool isNew = true;
  bool isDone = false;
  bool isDeleted = false;

  OrderListing(this._tableNumber, this._id);

  String get id => _id;

  String get tableNumber => _tableNumber;

  @override
  bool operator ==(Object o) => o is OrderListing && _id == o.id;

}