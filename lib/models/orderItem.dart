class OrderItem {

  String _state;
  String _meal;
  String _price;
  String _option;
  String _table;
  String _id;

  String get status => _state;

  String get meal => _meal;

  String get table => _table;

  String get option => _option;

  String get price => _price;

  String get state => _state;


  OrderItem(this._state, this._meal, this._price, this._option, this._table, this._id);

  Map toMap() {
    var map = new Map<String, dynamic>();
    map['state'] = this._state;
    map['meal'] = this._state;
    map['price'] = this._state;
    map['option'] = this._state;
    map['tableCode'] = this._table;
    map['tableCode'] = this._table;
    return map;
  }

  String get id => _id;

}