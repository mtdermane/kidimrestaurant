import 'package:KidimKitchen/pages/0_loginLayout.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'translations.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
  final String _serverAddress = 'https://kidim-backend.appspot.com/';
}

class _MyAppState extends State<MyApp> {
  Future<void> setServerAddress() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('serverAddress', widget._serverAddress);
  }

  @override
  void initState() {
    setServerAddress();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Kidim Kitchen',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      localizationsDelegates: [
        const TranslationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en', ''),
        const Locale('fr', ''),
      ],
      home: Scaffold(
        backgroundColor: Colors.orange[50],
        body: SafeArea(
          child: LoginPage(),
        ),
      ),
    );
  }
}
