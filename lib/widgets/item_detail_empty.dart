import 'package:flutter/material.dart';
import '../translations.dart';

class ItemDetailsEmpty extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(0, 100, 0, 0),
          child: Text(
            Translations.of(context).text('emptyMessage'),
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: 'Baskerville',
              fontSize: 25.0,
            ),
          ),
        ),
        Text(
          Translations.of(context).text('emptySubMessage'),
          textAlign: TextAlign.center,
          style: TextStyle(
            fontFamily: 'Baskerville',
            fontSize: 15.0,
          ),
        ),
      ],
    );
  }
}
