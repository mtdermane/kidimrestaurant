import '../models/orderListing.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

class ItemListing extends StatelessWidget {
  ItemListing({
    @required this.itemSelectedCallback,
    @required this.itemList,
    this.selectedItem,
  });

  final ValueChanged<int> itemSelectedCallback;
  final OrderListing selectedItem;
  final List<OrderListing> itemList;
  final String _table = "Table ";

  Widget trailingIcon(OrderListing listing) {
    if (listing.isDone) {
      return Icon(
        Icons.check,
        color: Colors.brown,
      );
    } else if (listing.isNew) {
      return Icon(
        Icons.new_releases,
        color: Colors.red,
      );
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    final double _dividerMargin = 20.0;
    return Container(
      color: Colors.orange[50],
      child: ListView.builder(
        padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
        itemCount: itemList.length,
        itemBuilder: (BuildContext context, int index) {
          return Column(
            children: <Widget>[
              ListTileTheme(
                selectedColor: Colors.red[600],
                textColor: Colors.brown[600],
                child: ListTile(
                  title: Text(
                    _table + (itemList[index]?.tableNumber ?? "N/A"),
                    style: TextStyle(
                      fontFamily: 'Baskerville',
                      fontSize: 18.0,
                    ),
                  ),
                  trailing: trailingIcon(itemList[index]),
                  onTap: () => itemSelectedCallback(index),
                  selected: selectedItem == itemList[index],
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(
                    vertical: 0.0, horizontal: _dividerMargin),
                child: Divider(
                  color: Colors.grey[400],
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
