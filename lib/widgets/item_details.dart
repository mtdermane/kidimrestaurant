import '../models/orderDetails.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import '../translations.dart';

class DisplayOption extends StatelessWidget {
  final Map _orderItem;

  DisplayOption(this._orderItem);

  final double paddingContainer = 8.0;

  Widget getTile(Map orderItem) {
    if (orderItem["option"] != "") {
      return ListTile(
        title: Text(
          orderItem["name"] ?? "",
          style: TextStyle(
            fontFamily: 'Baskerville',
            fontSize: 20.0,
          ),
        ),
        subtitle: orderItem["option"] != null ? Text(
          orderItem["option"] ?? "",
          style: TextStyle(
            fontFamily: 'Baskerville',
            fontSize: 15.0,
          ),
        ): null,
        trailing: Text(
          "\$" + orderItem["price"],
          style: TextStyle(
            fontFamily: 'Baskerville',
            fontSize: 20.0,
          ),
        ),
      );
    }
    return ListTile(
      title: Text(
        orderItem["name"],
        style: TextStyle(
          fontFamily: 'Baskerville',
          fontSize: 20.0,
        ),
      ),
      trailing: Text(
        "\$" + orderItem["price"],
        style: TextStyle(
          fontFamily: 'Baskerville',
          fontSize: 20.0,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        getTile(_orderItem),
        Container(
          padding:
              EdgeInsets.symmetric(vertical: 0.0, horizontal: paddingContainer),
          child: Divider(
            color: Colors.grey[800],
          ),
        )
      ],
    );
  }
}

class ItemDetails extends StatelessWidget {
  ItemDetails({
    @required this.orderIsDone,
    @required this.isInTabletLayout,
    @required this.item,
  });

  final bool isInTabletLayout;
  final OrderDetail item;
  final Function orderIsDone;

  double getTotal() {
    double total = 0;
    if (item != null) {
      item.order.forEach((orderItem) {
        total += double.parse(orderItem["price"]);
      });
    }
    return total;
  }

  @override
  Widget build(BuildContext context) {
    final Widget content = Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          width: 200,
          padding: EdgeInsets.symmetric(vertical: 50.0, horizontal: 0.0),
          child: RaisedButton(
            onPressed: () {
              this.orderIsDone();
            },
            color: Colors.red,
            child: Text(Translations.of(context).text('detailAction'),
              style: TextStyle(
                fontFamily: 'Baskerville',
                color: Colors.white,
                fontSize: 25.0,
              ),),
          ),
        ),
        Text(
          Translations.of(context).text('detailTotal') + ": \$" + getTotal().toString(),
          style: TextStyle(
            fontFamily: 'Baskerville',
            color: Colors.brown[600],
            fontSize: 30.0,
          ),
        ),
        Container(
          padding: EdgeInsets.fromLTRB(75.0, 10.0, 75.0, 0.0),
          child: ListView.builder(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemCount: item?.order?.length ?? 1,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  height: (item?.order != null &&
                          item?.order[index]["option"] == "")
                      ? 75
                      : 100,
                  child: DisplayOption(
                    item?.order != null
                        ? item?.order[index]
                        : {
                            "option": "No option",
                            "name": "No Meal",
                            "price": "No price"
                          },
                  ),
                );
              }),
        )
      ],
    );

    if (isInTabletLayout) {
      return Center(child: content);
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(item.seen.toString()),
      ),
      body: Center(child: content),
    );
  }
}
