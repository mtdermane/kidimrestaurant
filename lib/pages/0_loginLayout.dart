import 'package:flutter/material.dart';
import '0_loginWidgets/loginLayout.dart';
import '1_ordersPage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';


class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  bool _ready = false;
  String _placeId;

  Future<void> getLocalInformation() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String placeId = prefs.getString('placeId');

    setState(() {
      _ready = true;
    });

    if(placeId != null){
      setState(() {
        _placeId = placeId;
      });
    }
  }

  Widget launchLayout(){
    if(_ready){
      if(_placeId != "" && _placeId != null){
        return MasterDetailContainer();
      }
      else{
        return LoginLayout();
      }
    }
    return SpinKitPouringHourglass(
      color: Colors.red[600],
      size: 250.0,
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getLocalInformation();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.orange[50],
        body: launchLayout(),
    );
  }
}
