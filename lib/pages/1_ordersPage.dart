import 'dart:collection';
import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import '../models/orderListing.dart';
import '../models/orderDetails.dart';
import 'package:KidimKitchen/widgets/item_details.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../widgets/item_listing.dart';
import '../widgets/item_detail_empty.dart';
import '../models/posts.dart';

class MasterDetailContainer extends StatefulWidget {
  final refreshDuration = 10;
  @override
  _ItemMasterDetailContainerState createState() =>
      _ItemMasterDetailContainerState();
}

class _ItemMasterDetailContainerState extends State<MasterDetailContainer> with WidgetsBindingObserver{

  //variable that determines if the device is a tablet or a phone
  static const int kTabletBreakpoint = 600;

  OrderListing _selectedItem;
  OrderDetail _selectedItemDetail;

  Timer _getOrders;
  List<OrderListing> _listing = new List();
  List<OrderDetail> _details = new List();
  Set<String> _idInQueue = new HashSet();

  int _index = 0;

  ///layout in the case the device is a mobile
  Widget _buildMobileLayout() {
    Widget _getSelectedDetails(){
      if(_selectedItemDetail == null){
        return ItemDetailsEmpty();
      }
      return ItemDetails(
        isInTabletLayout: false,
        item: _selectedItemDetail,
      );
    }
    return ItemListing(
      itemSelectedCallback: (index) {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (BuildContext context) {
              return _getSelectedDetails();
            },
          ),
        );
      },
    );
  }

  ///get the order from the server
  void getMenu() async {

    final prefs = await SharedPreferences.getInstance();
    String serverAddress = prefs.getString('serverAddress');
    String placeId = prefs.getString('placeId');

    String excludedid = "";

    this._idInQueue.toList().forEach(
        (id) => {
          excludedid += "&_id[\$nin]=${id}",
        }
    );

    //getting url
    String url = "${serverAddress}orders/?seen=false&placeId=$placeId$excludedid";

    //calling the server request
    makePostRequest(url);

  }

  ///get the order from the server
  void updateOrder( String id ) async {

    final prefs = await SharedPreferences.getInstance();
    String serverAddress = prefs.getString('serverAddress');

    //getting url
    String url = "${serverAddress}orders/${id}";

    //calling the server request
    makeUpdateRequest(url);

  }

  ///Function used to create a post
  Future<void> makePostRequest(String url) async {

    final response = await get(url);

    final int statusCode = response.statusCode;

    if (statusCode < 200 || statusCode > 400) {
      throw new Exception("Error while fetching data");
    }

    //to put in a try catch FormatException
    try{
      List responseBody = jsonDecode(response.body)["data"];

      List<OrderListing> listing = new List();
      List<OrderDetail> details = new List();

      OrderListing aListing;
      OrderDetail aDetail;
      responseBody.forEach(
              (order) => {
                this._idInQueue.add(order["_id"]),
                aListing = new OrderListing(order["table"].toString(), order["_id"]),
                aDetail = new OrderDetail(order["_id"], order["placeId"], order["seen"], order["table"].toString(), order["order"]),
                if(order["seen"]){
                  aListing.isDone = true
                },
                listing.add(aListing),
                details.add(aDetail)
              }
      );
      setState(() {
        _listing.addAll(listing);
        _details.addAll(details);
      });
    }
    on FormatException catch(e){
      print(e);
    }
    on TypeError catch(e){
      print(e);
    }
  }


  ///Function used to create a post
  Future<void> makeUpdateRequest(String url) async {

    Map<String, String> headers = {"Content-type": "application/json"};
    String body = '{"seen": true}';

    Response response = await patch(url, headers: headers, body: body);

    final int statusCode = response.statusCode;

    if (statusCode < 200 || statusCode > 400) {
      throw new Exception("Error while fetching data");
    }
  }

  ///get the layout in the case we have a tablet
  ///The flex are the key for it to work
  Widget _buildTabletLayout() {
    Widget _getSelectedDetails(){
      if(_selectedItemDetail == null){
        return ItemDetailsEmpty();
      }
      return ItemDetails(
          orderIsDone: (){
            updateOrder(_listing[_index].id);
            if(!_listing[_index].isDone){
              setState((){
                _listing[_index].isDone = true;
              });
            }
            else{
              setState(() {
                _listing.removeAt(_index);
                _details.removeAt(_index);
                _selectedItemDetail = null;
              });
            }
          },
          isInTabletLayout: true,
          item: _selectedItemDetail
      );
    }
    return Row(
      children: <Widget>[
        Flexible(
          flex: 1,
          child: Material(
            elevation: 4.0,
            child: ItemListing(
              itemSelectedCallback: (index) {
                setState(() {
                  _index = index;
                  _listing[index].isNew = false;
                  _selectedItem = _listing[index];
                  _selectedItemDetail = _details[index];
                });
              },
              itemList: _listing,
              selectedItem: _selectedItem,
            ),
          ),
        ),
        Flexible(
          flex: 3,
          child: _getSelectedDetails()
        ),
      ],
    );
  }

  ///at the beginning we will connect to the server to get the order list
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    getMenu();
    const callInterval = const Duration(seconds:5);
    this._getOrders = new Timer.periodic(callInterval, (Timer t) => getMenu());
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if(state == AppLifecycleState.resumed) {

      //after a long cool down the array doesnt work properly
      //the solution here is to reset the array to keep the functionning correct
      //first create temporary list to store existing data
      List<OrderListing> tempListing = new List();
      List<OrderDetail> tempDetail = new List();
      Set<String> tempid = new HashSet();

      //import data from the existing structures
      tempListing.addAll(_listing);
      tempDetail.addAll(_details);
      tempid.addAll(_idInQueue);

      //not only clear but reset the exinsting ressources
      _listing = new List();
      _details = new List();
      _idInQueue = new HashSet();

      //reimport the list
      _listing = tempListing;
      _details = tempDetail;
      _idInQueue = tempid;

      //once the list are enabled, restart the timer
      const callInterval = const Duration(seconds:5);
      this._getOrders = new Timer.periodic(callInterval, (Timer t) => getMenu());
    }
    if(state == AppLifecycleState.paused){
      this._getOrders.cancel();
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget content;
    var shortestSide = MediaQuery.of(context).size.shortestSide;

    if (shortestSide < kTabletBreakpoint) {
      content = _buildMobileLayout();
    }
    else {
      content = _buildTabletLayout();
    }

    return Scaffold(
      backgroundColor: Colors.orange[50],
      appBar: AppBar(
        title: Text('Kidim Kitchen'),
        backgroundColor: Colors.red[800],
      ),
      body: content,
    );
  }
}
